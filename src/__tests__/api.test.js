const request = require("supertest");
const app =require("../index");

describe("GET /", () => {
    test("should return 200 OK", async done => {
        const result = await request(app).get("/");
        expect(result.status).toBe(200);
        done();
    });
});