const bodyParser=require("body-parser");
const express=require( "express");
var cors = require('cors');

// Create Express server
const app = express();

if (typeof localStorage === "undefined" || localStorage === null) {
  var LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./scratch');
}

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

// Express configuration
const port = process.env.PORT || 8880;
app.set("port", port);

const baseUrl =
    process.env.BASE_URL ||
    `http://localhost:${port}`;

app.get("/", function(req, res) {
    res.send( "Server is running!");
});

app.get("/healthcheck", function(req, res) {
    res.send("ok");
});

// This will be called when a user submits a new score
app.post("/submitScore", function(req, res)
{
  console.log('got request to post score')
  console.dir(req.body)
  localStorage.setItem('player', req.body);
  try {
    res.status(200).send("New player detail saved to database: " + req.body);
   }
    catch(err){
      res.status(400).send("Unable to save locally");
    }
});

app.get('/leaderboard', (req, res, next) => {
  console.log('got request for leaderboard')
  try {
    if (player === {}){
      res.status(200).send("There is no players yet");
    }
    else {
      let player = localStorage.getItem('player');
      res.status(200).send(player);
      next();
    }
  }
  catch(err){
    res.send(500, err)
  }
});

app.listen(port, function() {
    console.log(`Your app is listening on port: ${port}`);
});

module.exports= app;



